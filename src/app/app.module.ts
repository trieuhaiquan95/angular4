import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';


//routes
import { appRoutes } from './app.routes';
//module
import {PageAppModule} from'./pages/page.module'
//component
import { AppComponent } from './app.component';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    PageAppModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
