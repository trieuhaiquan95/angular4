import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { Routes, RouterModule } from '@angular/router';
import { Ng2SmartTableModule } from 'ng2-smart-table';
const homeRoutes: Routes = [
	{ path: '', redirectTo: 'list', pathMatch: 'full' },
	{ path: 'list', component: ProductComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(homeRoutes),
		Ng2SmartTableModule
	],
	declarations: [ProductComponent]
})
export class ProductModule { }