import {Component,OnInit } from '@angular/core'

@Component({
    selector: 'app-product',
    templateUrl:'./product.html'
})
export class ProductComponent implements OnInit {
    constructor(){}

    ngOnInit(){}
    settings = {
        columns: {
            id: {
                title: 'ID'
            },
            name: {
                title: 'Full Name'
            },
            username: {
                title: 'User Name'
            },
            email: {
                title: 'Email'
            }
        }
    };
}