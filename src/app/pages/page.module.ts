import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


//component
import { PageAppComponent } from './page.component';
import { AsideComponent } from './shared/aside/aside.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent} from './shared/footer/footer.component';
// This Routes
import { pageRoutes } from './page.routes';

@NgModule({
  declarations: [
    PageAppComponent,
    AsideComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forChild(pageRoutes)
  ],
  providers: [],
})
export class PageAppModule { }
