import { Routes } from '@angular/router';
import { PageAppComponent } from './page.component';

// Lazy loading routing module
export const pageRoutes: Routes = [
	{
		path: '', component: PageAppComponent, children: [
			{ path: '', redirectTo: '', pathMatch: 'full' },
			// localhost:4200/main/home
			{ path: '', loadChildren: './home/home.module#HomeModule' },
			{ path: 'product', loadChildren: './product/product.module#ProductModule' },
		]
	}
]