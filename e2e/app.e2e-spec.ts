import { WAREHOUSEWEBPage } from './app.po';

describe('warehouse-web App', () => {
  let page: WAREHOUSEWEBPage;

  beforeEach(() => {
    page = new WAREHOUSEWEBPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
